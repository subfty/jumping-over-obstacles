﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class GemSlot : MonoBehaviour {
    #region Data    
    protected FightConfig.GemType _Type = FightConfig.GemType.EMPTY;
    #endregion

    #region Game Objects
    [Header("Game Objects")]
    [SerializeField]
    protected Image _Gem;
    #endregion

    #region Properties
    public FightConfig.GemType GemType {
        get {
            return _Type;
        }
        set {
            _Gem.gameObject.SetActive(value != FightConfig.GemType.EMPTY);            

            switch (value) {
                case FightConfig.GemType.EMPTY:
                    
                    break;
                case FightConfig.GemType.REGULAR_0:                    
                    _Gem.color = Color.blue;
                    break;
                case FightConfig.GemType.REGULAR_1:
                    _Gem.color = Color.green;
                    break;
                case FightConfig.GemType.REGULAR_2:
                    _Gem.color = Color.yellow;
                    break;
                case FightConfig.GemType.REGULAR_3:
                    _Gem.color = Color.red;
                    break;
            }

            _Type = value;
        }
    }

    public Image Gem {
        private set {}
        get { return _Gem; }
    }
    #endregion

    protected void Awake() {
        GemType = _Type;
    }

}

