﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class JumpOverObstacles : FightPlaceholder {
    #region Game Object bindings
    [Header("Resources")]
    [SerializeField]
    private GameObject _Resources;
    [SerializeField]
    private GameObject _ObstaclePrefab;
    [SerializeField]
    private GameObject _JumpCloudPrefab;
    [Header("Container")]
    [SerializeField]
    private GameObject _ObstaclesContainer;    
    #endregion

    #region Config
    [Header("Config")]
    [SerializeField]
    DiffRangeProperty _ObstacleSpeed = new DiffRangeProperty(200.0f, 250.0f);    
    [SerializeField]    
    private DiffRangeProperty _BaseSpawnDelay = new DiffRangeProperty(1.5f, -0.5f);
    [SerializeField]
    [Range(0, 1)]
    private float _MaxSpawnDelayVariance = 0.2f;
    [SerializeField]
    [Range(0.0f, 10.0f)]
    float _JumpAcc;
    [SerializeField]
    [Range(0.0f, 10.0f)]
    float _Gravity;
    [SerializeField]
    [Range(0.0f, 0.75f)]
    float _MaxJumpDuration;
    [SerializeField]    
    DiffRangeProperty _ChanceForSingleHeads = new DiffRangeProperty();
    [SerializeField]
    DiffRangeProperty _ChanceForDoubleHeads = new DiffRangeProperty();
    [SerializeField]
    DiffRangeProperty _ChanceForTripleHeads = new DiffRangeProperty();
    #endregion    

    #region Properties
    float ObstacleSpeed {
        get { return _ObstacleSpeed.GetValue(GameDifficulty); }
    }

    float SpawnTime {
        get { return _SpawnDelay; }
    }

    float FloorLevel {
        get { return 70.0f; }
    }

    float DotRadius {
        get { return 20.0f; }
    }

    bool IsDotOnGround {
        get {
            return !_IsJumping && _TDot.anchoredPosition.y == FloorLevel + DotRadius;
        }
    }    

    float Gravity {
        get { return -_Gravity * 1000.0f; }
    }

    float JumpAcc {
        get {
            return _JumpAcc * 1000.0f; 
        }
    }
    #endregion

    #region Pools
    List<RectTransform> _OpponentsPool;
    List<RectTransform> _JumpCloudsPool;
    #endregion

    FightDot _Dot;
    RectTransform _TDot;
    float _LastSpawned;
    float _DotYVeloc = 0.0f;
    bool _IsJumping;    
    float _DotPosY = 0.0f;    
    float _DotTimeInAir = 0.0f;
    float _SpawnDelay;
    bool _FirstTimePressed = false;

    void RandNewSpawnDelay() {
        _SpawnDelay = 
            _BaseSpawnDelay.GetValue(GameDifficulty) + 
            _MaxSpawnDelayVariance * UnityEngine.Random.Range(0.0f, 1.0f);
    }

    protected override void Start() {
        base.Start();        

        // initialisation
        if (_Dot == null) {
            _Resources.gameObject.SetActive(false);

            _Dot = SpawnDot();
            _Dot.transform.SetParent(this.transform, false);
            _TDot = _Dot.GetComponent<RectTransform>();

            _OpponentsPool = new List<RectTransform>();
            _JumpCloudsPool = new List<RectTransform>();
        }

        if (_GemTypes != null)
            _Dot.SetGemTypes(_GemTypes);

        _LastSpawned = 0.0f;
        _IsJumping = false;        
        _DotTimeInAir = 0.0f;
        _SpawnDelay = 0.0f;
        _FirstTimePressed = false;

        _TDot.anchoredPosition = new Vector2(50, FloorLevel + DotRadius);
    }

    protected override void OnDestroy() {
        base.OnDestroy();

        RecyclePool(_OpponentsPool);
        RecyclePool(_JumpCloudsPool);  
    }

    protected override void GameUpdate() {
        base.GameUpdate();

        float delta = Time.deltaTime;

        // moving obstacles
        foreach (RectTransform t in _OpponentsPool) {
            if (!t.gameObject.activeSelf) continue;            

            t.anchoredPosition = 
                new Vector2(
                t.anchoredPosition.x - delta * ObstacleSpeed,
                t.anchoredPosition.y);

            if (t.anchoredPosition.x + t.sizeDelta.x < 0)
                Recycle<RectTransform>(t);                            
        }

        // spawning obstacles
        _LastSpawned += delta;
        while (_LastSpawned > SpawnTime) {
            _LastSpawned -= SpawnTime;

            int headNum = 1;
            float chance = _ChanceForSingleHeads.GetValue(GameDifficulty);
            if (UnityEngine.Random.Range(0.0f, 1.0f) < 
                _ChanceForDoubleHeads.GetValue(GameDifficulty) / 
                (chance + _ChanceForDoubleHeads.GetValue(GameDifficulty)))
                headNum = 2;
            chance += _ChanceForDoubleHeads.GetValue(GameDifficulty);
            if (UnityEngine.Random.Range(0.0f, 1.0f) < 
                _ChanceForTripleHeads.GetValue(GameDifficulty) / 
                (chance + _ChanceForTripleHeads.GetValue(GameDifficulty)))
                headNum = 3;
            chance += _ChanceForTripleHeads.GetValue(GameDifficulty);

            float pos = FightPlaceholder.PLAY_AREA_WIDTH;
            while (headNum-- > 0) {
                RectTransform head = GetRecycled<RectTransform>(_OpponentsPool, HeadFactory);
                HeadInit(head, pos, FloorLevel);
                pos += head.sizeDelta.x + 5.0f;
            }

            RandNewSpawnDelay();            
        }

        if (IsDotOnGround) {            
            _IsJumping = false;
            _DotTimeInAir = 0.0f;
            _DotYVeloc = 0.0f;
        } else {
            _DotTimeInAir += delta;
        }

        if (JustPressed)
            _FirstTimePressed = true;

        if (_IsJumping && IsPressed) {
            _DotYVeloc = JumpAcc;
            if (_DotTimeInAir > _MaxJumpDuration)
                _IsJumping = false;            
        }
        if (_FirstTimePressed && IsPressed && IsDotOnGround) {
            _IsJumping = true;
            _DotYVeloc = JumpAcc;
        }        

        if (!IsPressed)
            _IsJumping = false;

        _DotYVeloc += Gravity * delta;

        _DotPosY = _TDot.anchoredPosition.y;
        _DotPosY += delta * _DotYVeloc;

        _DotPosY = Mathf.Max(DotRadius + FloorLevel, _DotPosY);

        if (_DotPosY == DotRadius + FloorLevel && _TDot.anchoredPosition.y != DotRadius + FloorLevel)
            JumpCloudInit(
                GetRecycled(_JumpCloudsPool, JumpCloudFactory),
                _TDot.anchoredPosition.x,
                _DotPosY);                                

        _TDot.anchoredPosition = new Vector2(
            _TDot.anchoredPosition.x,
            _DotPosY);

        // checking for collisions
        foreach (RectTransform t in _OpponentsPool) {
            if (!t.gameObject.activeSelf) continue;            

            Rect r = new Rect(
                t.anchoredPosition.x,
                t.anchoredPosition.y,
                t.sizeDelta.x,
                t.sizeDelta.y);
            if (r.Contains(_TDot.anchoredPosition)) {
                GameFailed();
                break;
            }
        }
    }

    RectTransform HeadFactory() {
        RectTransform rTransf = Instantiate(_ObstaclePrefab).GetComponent<RectTransform>();
        rTransf.transform.SetParent(_ObstaclesContainer.transform, false);
        return rTransf;
    }

    RectTransform HeadInit(RectTransform head, float posX, float posY) {
        head.anchoredPosition = new Vector2(posX, posY);        
        return head;
    }

    RectTransform JumpCloudFactory() {
        RectTransform cloud = Instantiate(_JumpCloudPrefab).GetComponent<RectTransform>();
        cloud.transform.SetParent(this.transform, false);
        cloud.gameObject.SetActive(true);
        return cloud;
    }

    RectTransform JumpCloudInit(RectTransform cloud, float posX, float posY) {
        posY -= DotRadius * 1.5f;                
        Image cloudI = cloud.GetComponent<Image>();

        cloud.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        cloudI.color = new Color(cloudI.color.r, cloudI.color.g, cloudI.color.b, 1.0f);

        cloud.anchoredPosition = new Vector2(posX, posY);
        cloud
            .DOScaleX(2.0f, 0.4f)
            .SetEase(Ease.OutExpo);
        cloud
            .DOScaleY(1.5f, 0.4f)
            .SetEase(Ease.OutExpo);
        cloudI
            .DOFade(0.0f, 0.4f)
            .SetEase(Ease.OutSine)
            .OnComplete(new TweenCallback(() => {
                Recycle<RectTransform>(cloud);                
            }));
        return cloud;
    }
}

