﻿using DG.Tweening;
using Entitas;
using Entitas.Unity.VisualDebugging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using TinyMessenger;
using UnityEngine;

public partial class GameController : Singleton<GameController> {

    public static String LEVEL_BOOTSTRAPS_NAMESPACE = "LevelBootstrap";

    public delegate void PostLevelLoad();        

    [HideInInspector]
    public bool PauseGame = false;
    [SerializeField]
    [HideInInspector]
    protected bool _FightModeDebug = false;

    Systems _systems; 
    bool _PauseSystemExecution = false;

    protected override void Awake() {
        base.Awake();

        DOTween.Init(false);
        
        _systems = createSystems(Pools.pool);
        _systems.Initialize();
    }

    public override void OnDestroy() {
        base.OnDestroy();

        TinyTokenManager
            .Instance
            .UnregisterAll(this);
    }

    void Start() {
         StartGame();               
    }

    void StartGame() {
        Debug.LogWarning("Debug Mode - Level not loaded!");
    }

    void Update() { 
        if(!_PauseSystemExecution) {            
            _systems.Execute();                       
        }
    }    

    Systems createSystems(Pool pool) {        

        #if (UNITY_EDITOR)
        return new DebugSystems()        
        #else
        return new Systems()
        #endif                                                            
            // Instantiating                        
            .Add(pool.CreateSystem <AddGameObjectSystem> ())
            .Add(pool.CreateSystem <AddNestedViewSystem> ())      
            

            // Input
            // ORDER IMPORTANT!
            .Add(pool.CreateSystem <InputSystem>())

            // Destroy
            .Add(pool.CreateSystem <DestroySystem> ());
    }    
}
