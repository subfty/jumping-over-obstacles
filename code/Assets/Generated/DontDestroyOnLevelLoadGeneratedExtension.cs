namespace Entitas {
    public partial class Entity {
        static readonly DontDestroyOnLevelLoad dontDestroyOnLevelLoadComponent = new DontDestroyOnLevelLoad();

        public bool isDontDestroyOnLevelLoad {
            get { return HasComponent(ComponentIds.DontDestroyOnLevelLoad); }
            set {
                if (value != isDontDestroyOnLevelLoad) {
                    if (value) {
                        AddComponent(ComponentIds.DontDestroyOnLevelLoad, dontDestroyOnLevelLoadComponent);
                    } else {
                        RemoveComponent(ComponentIds.DontDestroyOnLevelLoad);
                    }
                }
            }
        }

        public Entity IsDontDestroyOnLevelLoad(bool value) {
            isDontDestroyOnLevelLoad = value;
            return this;
        }
    }

    public partial class Matcher {
        static IMatcher _matcherDontDestroyOnLevelLoad;

        public static IMatcher DontDestroyOnLevelLoad {
            get {
                if (_matcherDontDestroyOnLevelLoad == null) {
                    var matcher = (Matcher)Matcher.AllOf(ComponentIds.DontDestroyOnLevelLoad);
                    matcher.componentNames = ComponentIds.componentNames;
                    _matcherDontDestroyOnLevelLoad = matcher;
                }

                return _matcherDontDestroyOnLevelLoad;
            }
        }
    }
}
